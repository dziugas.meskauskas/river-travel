import L from 'leaflet';
import noUiSlider from 'nouislider';
import nemunas from '../coords/json/nemunas.json'
import neris from '../coords/json/neris.json'
import { getDistance, getAlongMarkers } from './utils';

const map = L.map('mapid').setView([54.687157, 25.279652], 8);
const controls = document.querySelector('#controls');
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

const nemunasLength = getDistance(nemunas[0]);
const nerisLength = getDistance(neris[0]);

const polylineNemunas = L.polyline(nemunas, {color: 'red'}).addTo(map).bindTooltip(`Nemunas - ${~~nemunasLength}km`, {permanent: true, sticky: true}).openTooltip();
const polylineNeris = L.polyline(neris, {color: 'blue'}).addTo(map).addTo(map).bindTooltip(`Neris - ${~~nerisLength}km`, {permanent: true}).openTooltip();


document.querySelector('.distance').innerHTML = ~~(nemunasLength + nerisLength)

const combinedTrack = [...neris[0], ...nemunas[0]];
const { markers } = getAlongMarkers(combinedTrack, {daysToComplete: 10});
const markersOnMap = [];

markers.forEach(({coords, fromStart, traveled}, index) => {
  markersOnMap.push(L.marker(coords).bindTooltip(`${~~fromStart}km. <br> Naktis: ${index + 1} `, { permanent: true, direction: 'top'}).addTo(map));
})

const daySlider = document.getElementById('daySlider');
const speedSlider = document.getElementById('speedSlider');
const dayCount = document.querySelector('.dayCount');
const speedCount = document.querySelector('.speedCount');
const hoursCount = document.querySelector('.hoursCount');

noUiSlider.create(daySlider, {
  start: [10],
  connect: true,
  range: {
    'min': 6,
    'max': 14
  },
  step: 1,
});
noUiSlider.create(speedSlider, {
  start: [5],
  connect: true,
  // tooltips: true,
  range: {
    'min': 2,
    'max': 10
  },
  step: 0.5,
});


let kmPerDay = 54;
let kayakSpeed = 5;

speedSlider.noUiSlider.on('update',([value]) => {
  speedCount.innerHTML = +value;
  hoursCount.innerHTML = Number(kmPerDay / +value).toPrecision(2);
});



daySlider.noUiSlider.on('end', ([value]) => {
  markersOnMap.forEach(marker => map.removeLayer(marker))
  dayCount.innerHTML = +value;
  const { markers, distanceToCoverPerDay } = getAlongMarkers(combinedTrack, {daysToComplete: +value});

  kmPerDay = distanceToCoverPerDay;
  hoursCount.innerHTML = Number(kmPerDay / kayakSpeed).toPrecision(2);


  markers.forEach(({ coords, fromStart }, index) => {
    markersOnMap.push(L.marker(coords).bindTooltip(`${~~fromStart}km. <br> Naktis: ${index + 1} `, { permanent: true, direction: 'top'}).addTo(map));
  })
});
