import * as turf from '@turf/turf'

export const getDistance = (polyline) => {
  const turfLine = turf.lineString(polyline);
  const polylineLength = turf.length(turfLine, { units: 'kilometers' });
  return polylineLength;
}

export const getAlongMarkers = (polyline, {daysToComplete}) => {
  const polylineDistance = getDistance(polyline);
  // console.log(polylineDistance)
  const distanceToCoverPerDay = polylineDistance / daysToComplete;
  // console.log(distanceToCoverPerDay)
  const markers = [];

  for (let i = 1; i <= daysToComplete; i++) {
    const line = turf.lineString(polyline);
    const options = { units: 'kilometers' };
    const distance = distanceToCoverPerDay * i;
    
    const along = turf.along(line, distance, options);
    markers.push({ coords: along.geometry.coordinates, fromStart: distanceToCoverPerDay * i, traveled: distanceToCoverPerDay })
  }
  
  return { markers, distanceToCoverPerDay };
}